<?php

// Autoload vendor classes
require "vendor/autoload.php";

if(getenv('DEBUG')){
    error_reporting(E_ALL);
    ini_set("display_startup_errors", "1");
    ini_set("display_errors", "1");
}

// Load .env values
(Dotenv\Dotenv::createImmutable(__DIR__))->load();

// Load project wide used classes
$dbConnection = (new App\DBAdapter\PDOAdapter())->getConnection();
$httpResponse = new App\Http\Response();
$sourceRepository = new App\Repository\Source($dbConnection);
$statisticsDataRepository = new App\Repository\StatisticsData($dbConnection);

