<?php
require 'bootstrap.php';

$statement = <<<EOS
    CREATE DATABASE  IF NOT EXISTS `aggregated_statistics` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
    USE `aggregated_statistics`;
    
    DROP TABLE IF EXISTS `sources`;
    /*!40101 SET @saved_cs_client     = @@character_set_client */;
    /*!40101 SET character_set_client = utf8 */;
    CREATE TABLE `sources` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `name` varchar(255) NOT NULL,
      `url` varchar(255) NOT NULL,
      `api_key` varchar(255) NOT NULL,
      `request_parameters` text,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
    EOS;

try {
    $createTable = $dbConnection->exec($statement);
    echo "Success!\n";
} catch (\PDOException $e) {
    exit($e->getMessage());
}