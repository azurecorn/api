Task
==============
Simple statistics API 

Technical Requirements
==============
PHP version 8  
MySQL 8

Install info
==============
git clone repo  
run 'composer install'  
setup your system DB credentials in .env file  
run 'dbseed.php' to import DB data structure  
run 'composer dump-autoload'  

API endpoint examples
==============
For managing sources:  

POST /source   example data: ['name' => 'google','url' => 'http://gstats.com','api_key' => 'XYZ', 'request_parameters' => "['a',b']"]  
PUT /source    example data: ['name' => 'google','url' => 'http://gstats.com','api_key' => 'XYZ', 'request_parameters' => "['a',b']"]  
DELETE /source/{id}  

For getting statistics data from external services:  

GET /statistics-data   optional filter data: ['period' => 'month']  

NOTE
==============
This project is not fully implementing features, the focus is on the architecture and the example