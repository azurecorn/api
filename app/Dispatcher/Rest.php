<?php

declare(strict_types=1);

namespace App\Dispatcher;

use App\Http\Response;
use App\Repository\Source;
use App\Repository\StatisticsData;
use App\Validator\Input;
use Exception;
use PDO;

/**
 * Class Rest
 * @package App\Dispatcher
 */
class Rest
{
    /**
     * @var string
     */
    private string $requestMethod;

    /**
     * @var null
     */
    private $entityController;

    /**
     * @var array
     */
    private array $uri;

    /**
     * @var Response
     */
    private Response $httpResponse;

    /**
     * Rest constructor.
     * @param string $requestMethod
     * @param array $uri
     * @param Input $validator
     * @param PDO $dbConnection
     * @param Response $httpResponse
     * @param Source $sourceRepository
     * @param StatisticsData $statisticsDataRepository
     */
    public function __construct(
        string $requestMethod,
        array $uri,
        Input $validator,
        PDO $dbConnection,
        Response $httpResponse,
        Source $sourceRepository,
        StatisticsData $statisticsDataRepository
    )
    {

        if (!$validator->check($uri)) {
            $this->httpResponse->notFoundResponse();
        }

        $entityController = null;

        try {
            $entity = ucfirst(strtolower($uri[1]));
            $entityControllerClass = 'Controller/' . $entity . 'Controller';

            //dynamic controller creation with all its dependencies
            $entityController = new $entityControllerClass(
                $dbConnection,
                $httpResponse,
                $sourceRepository,
                $statisticsDataRepository
            );
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        $this->requestMethod = $requestMethod;
        $this->uri = $uri;
        $this->entityController = $entityController;
    }

    /**
     * Dispatching API calls to proper controller and method
     */
    public function processRequest(): void
    {
        switch ($this->requestMethod) {
            case 'GET':
                $this->entityController->get($this->uri);
                break;
            case 'POST':
                $this->entityController->create($this->uri);
                break;
            case 'PUT':
                $this->entityController->update($this->uri);
                break;
            case 'DELETE':
                $this->entityController->delete($this->uri);
                break;
            default:
                $this->httpResponse->notFoundResponse();
                break;
        }
    }
}