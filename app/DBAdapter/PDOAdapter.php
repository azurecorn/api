<?php

declare(strict_types=1);

namespace App\DBAdapter;

use PDO;

/**
 * Class PDOAdapter
 * @package App\DBAdapter
 */
class PDOAdapter
{
    /**
     * @var PDO
     */
    private PDO $dbConnection;

    /**
     * PDOAdapter constructor.
     */
    public function __construct()
    {
        $host = getenv('DB_HOST');
        $db = getenv('DB_DATABASE');
        $user = getenv('DB_USERNAME');
        $pass = getenv('DB_PASSWORD');

        try {
            $this->dbConnection = new PDO("mysql:host={$host};dbname={$db},{$user},{$pass}");
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Returns connection object
     *
     * @return PDO
     */
    public function getConnection()
    {
        return $this->dbConnection;
    }
}