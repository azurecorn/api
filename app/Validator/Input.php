<?php

declare(strict_types=1);

namespace App\Validator;

/**
 * Sample skeleton class used for the validations
 *
 * Class Input
 * @package App\Validator
 */
class Input
{
    /**
     * This is just a sample skeleton method in which is possible to check and validate the API parameters data.
     * Should return false in a case of wrong request.
     * In the case below, as an example, we are just checking the entity, but but not the request parameters.
     *
     * @param array $uri
     * @return bool
     */
    public function check(array $uri): bool
    {
        $entity = $uri[1];

        if ($entity !== 'source' || $entity !== 'statistics-data') {
            return false;
        }

        if (!empty($uri)) {
            return true;
        }
    }
}