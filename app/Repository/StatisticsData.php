<?php

declare(strict_types=1);

namespace App\Repository;

use App\Http\Client;
use GuzzleHttp\Exception\RequestException;
use PDO;
use PDOException;

/**
 * Class Statistics
 */
class StatisticsData
{
    private ?\PDO $dbConnection = null;

    /**
     * Source constructor.
     * @param PDO $dbConnection
     */
    public function __construct(PDO $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }
    /**
     * Argument $params is used for response data filtering, like by current month ($filter['month'])
     *
     * @param array $filter
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getStatistics($filter = []): array
    {
        try {
            $this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sql = 'SELECT url FROM sources';

            $statement = $this->dbConnection->query($sql);

            $sources = $statement->fetchAll(PDO::FETCH_ASSOC);

            $statistics = [];

            //foreach is an example, it would be good to do this logic by queue
            /** @var $source \App\Model\Source */
            foreach ($sources as $source){
                try {
                    $httpClient = new Client();

                    /** @var array $requestParameters */
                    $requestParameters = $source->getRequestParameters();
                    $token = ['token' => $source->getApiKey()];

                    //$filter example is expected to be in format ['period' => month]
                    $query = array_merge($token, $requestParameters, $filter);

                    //this is an example request to some external statistics server
                    $request = $httpClient->instance->request('GET', $source->getUrl(), [
                        'query' => [
                            $query
                        ]
                    ]);

                    $externalServiceResponse = json_decode($request->getBody()->read(1024));

                    $statistics[] = $externalServiceResponse;

                } catch (RequestException $e) {
                    if ($e->hasResponse()) {
                        echo json_decode($e->getResponse()->getBody()->read(1024));
                    }
                }
            }

            return $statistics;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
