<?php

declare(strict_types=1);

namespace App\Repository;

use App\DBAdapter\PDOadapter;
use App\Model\Source as SourceModel;
use PDO;
use PDOException;

/**
 * Class Source
 */
class Source
{
    private ?\PDO $dbConnection = null;

    /**
     * Source constructor.
     * @param PDO $dbConnection
     */
    public function __construct(PDO $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    public function findById(int $id): SourceModel
    {
        try {
            $this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sql = 'SELECT * FROM sources WHERE id=:id';

            $statement = $this->conn->prepare($sql);

            $statement->bindValue("id", $id, PDO::PARAM_INT);

            $statement->execute();

            return $statement->fetchAll(PDO::FETCH_OBJ);

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function save(SourceModel $source): bool
    {
        try {
            $this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sql = 'INSERT INTO sources (name,url,request_parameters)
					    VALUES ( :name, :url, :request_parameters);';

            $statement = $this->conn->prepare($sql);

            $statement->bindValue("name", $source->getName(), PDO::PARAM_STR);
            $statement->bindValue("url", $source->getUrl(), PDO::PARAM_STR);
            $statement->bindValue("request_parameters", $source->getRequestParameters(), PDO::PARAM_STR);

            $statement->execute();

            return true;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }

    }

    public function update(SourceModel $source): bool
    {
        try {
            $this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sql = 'UPDATE sources 
                    SET name=:name, url=:url, request_parameters=:request_parameters 
                    WHERE id=:id';

            $statement = $this->conn->prepare($sql);

            $statement->bindValue("id", $source->getId(), PDO::PARAM_INT);
            $statement->bindValue("name", $source->getName(), PDO::PARAM_STR);
            $statement->bindValue("url", $source->getUrl(), PDO::PARAM_STR);
            $statement->bindValue("request_parameters", $source->getRequestParameters(), PDO::PARAM_STR);

            $statement->execute();

            return true;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function delete(int $id): array
    {
        try {
            $this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sql = 'DELETE FROM sources WHERE id=:id';

            $statement = $this->conn->prepare($sql);

            $statement->bindValue('id',$id, PDO::PARAM_INT);

            $statement->execute();

            return true;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
