<?php

declare(strict_types=1);

namespace App\Http;

/**
 * Interface ResponseInterface
 * @package App\Http
 */
interface ResponseInterface
{
    public const HTTP_OK = 200;
    public const HTTP_BAD_REQUEST = 400;

    /**
     * Signature for a method which packs a response
     *
     * @param int $status
     * @param string $body
     * @return mixed
     */
    public function send(string $body, int $status);

    /**
     * @param int $status
     * @param string $body
     * @return mixed
     */
    public function sendErrorResponse(int $status, string $body);

    /**
     * Handy response for often used HTTP 404 error response
     *
     * @return mixed
     */
    public function notFoundResponse();
}
