<?php

declare(strict_types=1);

namespace App\Http;

use GuzzleHttp\Client as GuzzleClient;

/**
 * Class Client holds the HTTP client. Currently GuzzleClient is used as an example.
 * @package App\Http
 */
class Client
{
    /**
     * @var GuzzleClient
     */
    public GuzzleClient $instance;

    /**
     * Client constructor.
     */
    public function __construct()
    {
        $this->instance = new GuzzleClient();
    }
}