<?php

declare(strict_types=1);

namespace App\Http;

use Exception;

/**
 * Class Response
 * @package App\Http
 */
class Response implements ResponseInterface
{
    /**
     * Adds HTTP response code and packs a body for a response for frontend
     *
     * @param int $status
     * @param null $body
     * @return mixed|void
     */
    public function send($body = null, int $status = ResponseInterface::HTTP_OK)
    {
        http_response_code($status);

        try {
            $response = [
                'error' => 'false',
                'message' => 'success',
                'data' => $body,
            ];

            echo json_encode($response, JSON_THROW_ON_ERROR, 512);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param int $status
     * @param null $body
     */
    public function sendErrorResponse(int $status, $body = null): void
    {
        http_response_code($status);

        $response = [
            'error' => 'true',
            'message' => 'something is wrong',
            'data' => $body,
        ];

        echo json_encode($response, JSON_THROW_ON_ERROR, 512);
    }

    /**
     * @return mixed|void
     */
    public function notFoundResponse()
    {
        http_response_code(ResponseInterface::HTTP_BAD_REQUEST);

        $response = [
            'error' => 'true',
            'message' => '404 - not found',
            'data' => [],
        ];

        echo json_encode($response, JSON_THROW_ON_ERROR, 512);
    }
}