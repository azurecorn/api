<?php

declare(strict_types=1);

namespace App\Model;

/**
 * Class Statistics
 */
class StatisticsData
{
    /**
     * @var array
     */
    private array $aggregatedStatistics = [];

    /**
     * @param array $aggregatedStatistics
     */
    public function setAggregatedStatistics(array $aggregatedStatistics): void
    {
        $this->aggregatedStatistics = $aggregatedStatistics;
    }

    /**
     * @return array
     */
    public function getAggregatedStatistics(): array
    {
        return $this->aggregatedStatistics;
    }

}
