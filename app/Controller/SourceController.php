<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\Source as RepositorySource;
use App\Model\Source as ModelSource;
use App\Http\Response as HttpResponse;
use App\Repository\StatisticsData;
use PDO;

/**
 * Class SourceController
 */
class SourceController
{
    /**
     * @var PDO
     */
    private PDO $dbConnection;

    /**
     * @var RepositorySource
     */
    private RepositorySource $sourceRepository;

    /**
     * @var HttpResponse
     */
    private HttpResponse $httpResponse;


    /**
     * @var StatisticsData
     */
    private StatisticsData $statisticsDataRepository;

    /**
     * SourceController constructor.
     * @param $dbConnection
     * @param $httpResponse
     * @param $sourceRepository
     * @param $statisticsDataRepository
     */
    public function __construct(
        $dbConnection,
        $httpResponse,
        $sourceRepository,
        $statisticsDataRepository)
    {
        $this->dbConnection = $dbConnection;
        $this->httpResponse = $httpResponse;
        $this->sourceRepository = $sourceRepository;
        $this->statisticsDataRepository = $statisticsDataRepository;
    }

    /**
     * @param array $data
     */
    public function insert(array $data): void
    {
        $source = new ModelSource();

        $source->setName($data['name']);
        $source->setRequestParameters($data['request_parameters']);
        $source->setUrl($data['url']);


        $this->sourceRepository->save($source);

        $this->httpResponse->send(['stored' => $source->getId()]);
    }

    /**
     * @param $data
     */
    public function update($data): void
    {
        $source = $this->sourceRepository->findById($data['id']);

        $source->setName($data['name'] ?? $source->getName());
        $source->setRequestParameters($data['request_parameters'] ?? $source->getRequestParameters());
        $source->setUrl($data['url'] ?? $source->getUrl());

        $this->sourceRepository->update($source);

        $this->httpResponse->send(['updated' => $source->getId()]);

    }

    /**
     * @param $id
     */
    public function delete($id): void
    {
        $this->sourceRepository->delete($id);

        $this->httpResponse->send(['deleted' => $id]);

    }
}