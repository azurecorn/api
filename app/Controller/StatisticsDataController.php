<?php

declare(strict_types=1);

namespace App\Controller;

use App\Http\Response as HttpResponse;
use App\Repository\Source as RepositorySource;
use App\Repository\StatisticsData;

/**
 * Class SourceController
 */
class StatisticsDataController
{
    /**
     * @var PDO
     */
    private PDO $dbConnection;

    /**
     * @var RepositorySource
     */
    private RepositorySource $sourceRepository;

    /**
     * @var HttpResponse
     */
    private HttpResponse $httpResponse;


    /**
     * @var StatisticsData
     */
    private StatisticsData $statisticsDataRepository;

    /**
     * SourceController constructor.
     * @param $dbConnection
     * @param $httpResponse
     * @param $sourceRepository
     * @param $statisticsDataRepository
     */
    public function __construct(
        $dbConnection,
        $httpResponse,
        $sourceRepository,
        $statisticsDataRepository)
    {
        $this->dbConnection = $dbConnection;
        $this->httpResponse = $httpResponse;
        $this->sourceRepository = $sourceRepository;
        $this->statisticsDataRepository = $statisticsDataRepository;
    }

    /**
     * @param $filter
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get($filter){
        $aggregatedStatistics = $this->statisticsDataRepository->getStatistics($filter);

        $this->httpResponse->send(['aggregated_statistics' => $aggregatedStatistics]);
    }
}